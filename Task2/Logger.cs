﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Task2.Interfaces;

namespace Task2
{
  /// <summary>
  /// Логирование.
  /// </summary>
  public class Logger : ILog
  {
    #region Поля и свойства
    /// <summary>
    /// Путь до файла.
    /// </summary>
    private readonly string filePath;
    #endregion    
    #region Методы
    public void Write(string text)
    {
      using (var streamWriter = new StreamWriter(this.filePath))
      {
        try
        {
          streamWriter.WriteLine($"{text}");
        }
        catch
        {
          // Запишем инфу о том что произошла ошибка.
          const string typeMessage = "Exception";
          const string message = "Ошибка записи лога в файл.";
          Debugger.Log(0, typeMessage, message);
        }
      }
    }
    #endregion
    #region Конструкторы
    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="filePath"></param>
    public Logger(string filePath)
    {
      this.filePath = filePath;
    }
    #endregion
  }
}
