﻿using System;

namespace Task2
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("Ожидайте выполнение программы...");
      // Вызов метода выполнения задания.
      new TaskReflection().Main();
      Console.WriteLine("Выполнение завершено.");
      Console.ReadLine();
    }
  }
}
