﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Task2.Interfaces;

namespace Task2
{
  /// <summary>
  /// Задание 2.
  /// </summary>
  internal class TaskReflection
  {
    #region Поля и свойства
    /// <summary>
    /// Путь до сборки.
    /// </summary>
    private readonly string pathToAssembly = @"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.7.2\System.Windows.Forms.dll";
    /// <summary>
    /// Полное имя до MessageBox.
    /// </summary>
    private readonly string typeName = "System.Windows.Forms.MessageBox";
    /// <summary>
    /// Путь до файла куда писать результат.
    /// </summary>
    private readonly string logPath = "result.txt";
    /// <summary>
    /// Объект логгера.
    /// </summary>
    private readonly ILog logger;
    #endregion    
    #region Методы
    /// <summary>
    /// Основной метод вызова старта выполнения задания.
    /// </summary>
    public void Main()
    {
      // Задание1.Загружаем сборку.
      var assembly = this.LoadAssembly(this.pathToAssembly);
      //  Получаем тип.
      var type = this.GetType(assembly, typeName);
      // Задание2. Получаем все методы типа.
      var methods = type.GetMethods();
      // Выводим список всех не статических методово.
      this.GetAllNonStaticMethodsAndWriteToLog(methods);
      // Вызываем метод отправки сообщения
      this.InvokeShowMethod(methods);
    }
    /// <summary>
    /// Загрузить сборку.
    /// </summary>
    /// <param name="pathToAssemly">Путь до сборки.</param>
    private Assembly LoadAssembly(string pathToAssemly)
    {
      if (!File.Exists(pathToAssemly))
      {
        return null;
      }
      return Assembly.LoadFrom(pathToAssemly);
    }
    /// <summary>
    /// Получить тип из сборки.
    /// </summary>
    /// <param name="typeName">Имя типа.</param>
    private Type GetType(Assembly assembly, string typeName)
    {
      return assembly.GetType(typeName);
    }
    /// <summary>
    /// Получить все не статические методы и записываем их в лог.
    /// </summary>
    /// <param name="methods">Список методов из которых надо осуществить поиск.</param>
    private void GetAllNonStaticMethodsAndWriteToLog(IEnumerable<MethodInfo> methods)
    {      
      var methodsNonStatic = methods.Where(t => !t.IsStatic).ToList();
      var join = string.Join("\n", methods.Select(t => t.Name));
      this.logger.Write(join);
    }
    /// <summary>
    /// Найти и вызвать метод Send. 
    /// </summary>
    /// <param name="methods">Список методов из которых надо найти нужный.</param>
    private void InvokeShowMethod(IEnumerable<MethodInfo> methods)
    {
      const string sendMethodName = "Show";
      const string message = "Текст в диалоговом окне!";
      const int paramsLength = 1; // заранее знаем что в данном случае нам нужен метод с колличеством параметров равным 1
      var sendMethod = methods.FirstOrDefault(t => t.Name.Equals(sendMethodName) && t.GetParameters().Length == paramsLength);
      sendMethod.Invoke(this, new object[] { message });
    }
    #endregion
    #region Конструкторы
    /// <summary>
    /// Конструктор.
    /// </summary>
    public TaskReflection()
    {
      logger = new Logger(logPath);
    }
    #endregion
  }
}
