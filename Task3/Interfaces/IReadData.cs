﻿using System;
using System.Collections.Generic;

namespace Task3.Interfaces
{
  /// <summary>
  /// Чтение данных.
  /// </summary>
  internal interface IReadData: IDisposable
  {
    /// <summary>
    /// Чтение файла.
    /// </summary>
    /// <returns>Возвращаем кортеж с полученными данными.</returns>
    IEnumerable<Tuple<string, string>> Read();
  }
}
