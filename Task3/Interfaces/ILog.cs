﻿
namespace Task3.Interfaces
{
  /// <summary>
  /// Логирование.
  /// </summary>
  internal interface ILog
  {
    /// <summary>
    /// Записать текст.
    /// </summary>
    /// <param name="text">Текст для записи.</param>
    void Write(string text);
  }
}
