﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Task3.Interfaces;

namespace Task3
{
  /// <summary>
  /// Задание3. 
  /// </summary>
  internal class TaskExcel
  {
    #region Поля и свойства
    /// <summary>
    /// Путь до файла Log.
    /// </summary>
    readonly string pathFileLog = "result.txt";
    /// <summary>
    /// Имя excel файла. 
    /// </summary>
    readonly string fileNameExcel = "products.xlsx";
    /// <summary>
    /// Путь до excel файла.
    /// </summary>
    readonly string pathToExcelFile;
    #endregion    
    #region Методы
    /// <summary>
    /// Основной метод для запуска выполнения задания.
    /// </summary>
    internal void Main()
    {
      var logger = new Logger(this.pathFileLog);
      using (IReadData reader = new ExcelReader(this.pathToExcelFile))
      {
        try
        {
          var resultReader = reader.Read();

          var products = resultReader
            .Select(t => Product.Init(t.Item1, t.Item2))
            .Where(t => t.Cost > 2000)
            .OrderBy(t => t.Name);

          var join = string.Join("\n", products.Select(t => $"{t.Name} - {t.Cost}"));
          logger.Write(join);

          TestJoin(products);
          TestStringBuilder(products);
        }
        catch (Exception ex)
        {
          // Запишем инфу о том что произошла ошибка.
          const string typeMessage = "Exception";
          Debugger.Log(0, typeMessage, ex.Message);
        }
      }
    }
    /// <summary>
    /// Тестирование производительности join.
    /// </summary>
    /// <param name="products">Список продуктов.</param>
    private void TestJoin(IEnumerable<Product> products)
    {
      var timeListJoin = new List<long>();
      var sw = Stopwatch.StartNew();
      for (int i = 0; i < 1000; i++)
      {
        sw.Restart();
        var join = string.Join("\n", products.Select(t => $"{t.Name} - {t.Cost}"));
        timeListJoin.Add(sw.ElapsedMilliseconds);
      }
      Console.WriteLine($"Time test join: {timeListJoin.Average()}"); ;
    }

    /// <summary>
    /// Тестирование производительности StringBUilder.
    /// </summary>
    /// <param name="products">Список продуктов.</param>
    private void TestStringBuilder(IEnumerable<Product> products)
    {
      var sb = new StringBuilder();
      var timeListSB = new List<long>();
      var sw = Stopwatch.StartNew();
      for (int i = 0; i < 1000; i++)
      {
        sw.Restart();
        foreach (var product in products)
        {
          sb.AppendLine($"{product.Name} - {product.Cost}");
        }
        var sbString = sb.ToString();
        timeListSB.Add(sw.ElapsedMilliseconds);
      }
      Console.WriteLine($"Time test SB: {timeListSB.Average()}"); ;
    }

    #endregion
    #region Конструкторы
    /// <summary>
    /// Конструктор.
    /// </summary>
    public TaskExcel()
    {
      // Генерим путь.
      this.pathToExcelFile = Path.Combine(Environment.CurrentDirectory, this.fileNameExcel);
    }
    #endregion
  }
}
