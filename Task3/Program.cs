﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task3.Interfaces;

namespace Task3
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("Ожидайте выполнения задания...");
      new TaskExcel().Main();
      Console.WriteLine("Выполнение задания завершено.");
      Console.Read();
    }
  }
}
