﻿using System;
using System.IO;
using Task3.Interfaces;

namespace Task3
{
  /// <summary>
  /// Логирование.
  /// </summary>
  sealed internal class Logger : ILog
  {
    #region Поля и свойства
    /// <summary>
    /// Путь до файла.
    /// </summary>
    private readonly string filePath;
    #endregion   
    #region Методы
    public void Write(string text)
    {
      using (var streamWriter = new StreamWriter(this.filePath))
      {
        try
        {
          streamWriter.WriteLine($"{text}");
        }
        catch
        {
          const string message = "Ошибка записи лога в файл.";
          throw new Exception(message);
        }
      }
    }
    #endregion
    #region Конструкторы
    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="filePath">Путь до файла.</param>
    public Logger(string filePath)
    {
      this.filePath = filePath;
    }
    #endregion
  }
}
