﻿namespace Task3
{
  /// <summary>
  /// Продукт.
  /// </summary>
 internal class Product
  {
    /// <summary>
    /// Имя продукта.
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Цена продукта.
    /// </summary>
    public int Cost { get; set; }

    /// <summary>
    /// Создаем продукт.
    /// </summary>
    /// <param name="name">Имя продукта.</param>
    /// <param name="cost">Цена продукта. </param>
    /// <returns>Сущность продукт.</returns>
   static internal Product Init(string name, string cost)
    {
      if (!int.TryParse(cost, out int costConvert))
      {
        return null;
      }
      return new Product
      {
        Name = name,
        Cost = costConvert
      };
    }
  }
}
