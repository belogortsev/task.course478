﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using Task3.Interfaces;

namespace Task3
{
  /// <summary>
  /// Чтение Excel файла.
  /// </summary>
  sealed internal class ExcelReader : IReadData
  {
    #region Поля и свойства
    /// <summary>
    /// Путь до файла.
    /// </summary>
    private readonly string pathToFile = @"C:\Users\Belogortsev_AA\source\repos\Course478.FinalTest\Task3\products.xlsx";
    /// <summary>
    /// Рабочая книга ексель.
    /// </summary>
    private Workbook workbook;
    /// <summary>
    /// Приложение ексель.
    /// </summary>
    private Application excel;
    #endregion    
    #region Методы
    public IEnumerable<Tuple<string, string>> Read()
    {
      this.workbook = this.OpenWorkbook(this.pathToFile);
      if (this.workbook == null)
      {
        const string message = "Не удалось прочитать файл  excel: ";
        throw new Exception($"{message} {this.pathToFile}.");
      }
      var worksheet = this.ReadWorksheet(this.workbook);
      if (worksheet == null)
      {
        const string message = "Не удалось прочитать страницу в рабочей книге.";
        throw new Exception(message);
      }
      var ceels = this.ReadCells(worksheet);
      return ceels;
    }

    

    /// <summary>
    /// Открываем рабочую книгу.
    /// </summary>
    /// <param name="path">Путь до файла.</param>
    /// <returns>Объект Workbook.</returns>
    private Workbook OpenWorkbook(string path)
    {
      try
      {
        Workbook workbook = this.excel.Workbooks.Open(path);
        return workbook;
      }
      catch (Exception)
      {
        return null;
      }
    }

    /// <summary>
    /// Чтение первого листа.
    /// </summary>
    /// <param name="workbook"></param>
    private Worksheet ReadWorksheet(Workbook workbook)
    {
      try
      {
        const int sheetNumber = 1; // интересна только первая страница
        var sheet = workbook.Worksheets[sheetNumber];
        return sheet;
      }
      catch (Exception)
      {
        return null;
      }
    }

    /// <summary>
    /// Читаем ячейки.
    /// </summary>
    /// <param name="worksheet"></param>
    /// <returns>Возвращаем список ячеек.</returns>
    private IEnumerable<Tuple<string, string>> ReadCells(Worksheet worksheet)
    {
      const bool flagStop = false;
      const int firstColumn = 1; // столбец с именем 
      const int secondColumn = 2; // столбец со стоимостью
      int currentRow = 2; // начинаем со второй строки
      while (!flagStop)
      {
        var cell1 = worksheet.Cells[currentRow, firstColumn].Value?.ToString();
        var cell2 = worksheet.Cells[currentRow, secondColumn].Value?.ToString();
        if (cell1 == null || cell2 == null)
        {
          break;
        }
        currentRow++;
        yield return new Tuple<string, string>(cell1, cell2);
      }
    }


    #endregion
    #region Конструкторы
    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="path">Путь до файла.</param>
    public ExcelReader(string path)
    {
      this.pathToFile = path;
      this.excel = new Application();
    }
    /// <summary>
    /// Деструктор.
    /// </summary>
    public void Dispose()
    {
      try
      {
        workbook.Close(false, null, null);
        this.excel.Quit();
      }
      catch
      {

      }
    }
    #endregion
  }
}
