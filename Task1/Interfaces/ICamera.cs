﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Interfaces
{
  /// <summary>
  /// Модуль камеры.
  /// </summary>
  internal interface ICamera
  {
    /// <summary>
    /// Событие которое возникает после создания снимка.
    /// </summary>
    event EventHandler<PhotoEventArgs> NewPhoto;

    /// <summary>
    /// Сделать фото.
    /// </summary>
    void TakePhoto();
  }
}
