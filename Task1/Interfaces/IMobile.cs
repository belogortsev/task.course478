﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Interfaces
{
  interface IMobile
  {
    /// <summary>
    /// IMEI  телефона.
    /// </summary>
    int IMEI { get; }
    /// <summary>
    /// Номер симкарты.
    /// </summary>
    int NumberSim { get; set; }
    /// <summary>
    /// Пользователи.
    /// </summary>
    IEnumerable<User> Users { get; set; }

    /// <summary>
    /// Подключение телефона к вышке сотовой связи.
    /// </summary>
    void Connect();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="number"></param>
    void Call(Number number);
    /// <summary>
    /// Выбранный номер
    /// </summary>
    /// <param name="user">Пользователь которому нужно позвонить.</param>
    void Call(User user);


  }
}
