﻿using System;
using Task1.Interfaces;

namespace Task1
{
  /// <summary>
  /// Телефон с камерой.
  /// </summary>
  internal class MobileWithCamera : Mobile
  {
    #region Поля и свойства
    /// <summary>
    /// Модуль камеры.
    /// </summary>
    private ICamera camera;
    #endregion
    #region Методы
    private void Camera_NewPhoto(object sender, PhotoEventArgs e)
    {
      this.SavePhoto(e.Photo);
    }
    /// <summary>
    /// Сохраняем фото куда-то
    /// </summary>
    /// <param name="photo">Фото.</param>
    private void SavePhoto(Photo photo)
    {
      Console.WriteLine("Получить фото откуда-то.");
    }
    #endregion    
    #region Конструкторы
    /// <summary>
    /// Конструктор.
    /// </summary>
    public MobileWithCamera()
    {
      this.camera = new Camera();
      this.camera.NewPhoto += Camera_NewPhoto;
    }
    #endregion    
  }
}
