﻿namespace Task1
{
  /// <summary>
  /// Пользователь.
  /// </summary>
  internal class User
  {
    /// <summary>
    /// Номер пользователя.
    /// </summary>
    public Number Number { get; set; }
    /// <summary>
    /// ФИО пользователя.
    /// </summary>
    public string FullName { get; set; }
  }
}
