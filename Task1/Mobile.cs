﻿using System;
using System.Collections.Generic;
using Task1.Interfaces;

namespace Task1
{
  internal class Mobile : IMobile
  {
    #region Поля и свойства
    /// <summary>
    /// IMEI номер телефона.
    /// </summary>
    public int IMEI { get; private set; }
    /// <summary>
    /// Номер сим карты.
    /// </summary>
    public int NumberSim { get; set; }
    /// <summary>
    /// Телефоная книга.
    /// </summary>
    public IEnumerable<User> Users { get; set; }
    #endregion
    #region Методы
    public virtual void Connect()
    {
      Console.WriteLine("Базовый коннект!");
    }

    public void Call(User user)
    {
      this.Call(user.Number);
    }

    public void Call(Number number)
    {
      Console.WriteLine("Позвонить куда-то!");
    }
    #endregion










  }
}
