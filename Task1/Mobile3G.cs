﻿using System;

namespace Task1
{
  /// <summary>
  /// Телефон с 3g.
  /// </summary>
  internal class Mobile3G : Mobile
  {
    public override void Connect()
    {
      Console.WriteLine("Коннект для телефона с 3g.");
    }
  }
}
