﻿using System;
using Task1.Interfaces;

namespace Task1
{
  internal class Camera : ICamera
  {
    public event EventHandler<PhotoEventArgs> NewPhoto;
    public void TakePhoto()
    {
      if (this.NewPhoto != null)
      {
        this.NewPhoto.Invoke(this, new PhotoEventArgs { Photo = new Photo() });
      }
    }
  }
}
