﻿namespace Task1
{
  /// <summary>
  /// Класс события хранящий аргументы фотою
  /// </summary>
  public class PhotoEventArgs
  {
    /// <summary>
    /// Фото.
    /// </summary>
    internal Photo Photo { get; set; }
  }
}
